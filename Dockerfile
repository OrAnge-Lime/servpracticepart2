FROM python:latest

USER 0

RUN apt-get update

RUN pip3 install pymongo

ARG PROJECT=client
ARG PROJECT_DIR=/${PROJECT}
RUN mkdir -p $PROJECT_DIR
WORKDIR $PROJECT_DIR

CMD python3 main.py