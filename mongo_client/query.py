def find_dates(coll):
    return coll.find({},{"_id":False, 
        "gradebook.subj1.exam_date": True,
        "gradebook.subj2.exam_date": True,
        "gradebook.subj3.exam_date": True
    })