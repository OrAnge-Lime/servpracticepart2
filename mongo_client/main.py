import os
from pymongo import MongoClient
from init_mongo import init
from query import find_dates

OUTPUT_PATH = "output"

client = MongoClient(
        host = ["mongo:27017"],
        serverSelectionTimeoutMS = 6000,
        username = "root",
        password = "example",
    )

students_collection = client.admin.students

init(students_collection)

result = find_dates(students_collection)

print("BRAINIACS ARE:")

os.makedirs(OUTPUT_PATH, exist_ok=True)
with open(os.path.join(OUTPUT_PATH, "res.txt"), "w+") as file:
    for doc in result:
        print(doc)
        file.write(str(doc))
        file.write("\n")