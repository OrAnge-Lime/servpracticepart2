# Задание 2.
## Установка:
1) Установите пакет через pip: `pip install servpracticepart2 --extra-index-url https://gitlab.com/api/v4/projects/41743165/packages/pypi/simple --no-deps` 

## Выполнение:
В корневой папке выполните команду `python3 -m servpracticepart2/main.py`, результат будет находиться в текстовом файле `output/res.txt`.